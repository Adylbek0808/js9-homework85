import React from 'react';
import { Route, Switch } from 'react-router';
import { Container, CssBaseline } from '@material-ui/core';


import AppToolbar from './components/UI/AppToolbar/AppToolbar';
import Artists from './containers/Artists/Artists'
import Albums from './containers/Albums/Albums';
import Tracks from './containers/Tracks/Tracks';



const App = () => (
  <>
    <CssBaseline />
    <header>
      <AppToolbar />
    </header>
    <main>
      <Container maxWidth="xl">
        <Switch>
          <Route path="/" exact component={Artists} />
          <Route path={'/albums'} component={Albums}/>
          <Route path="/tracks" component={Tracks}/>

        </Switch>
      </Container>
    </main>
  </>
)

export default App;
