import { CircularProgress, Grid, makeStyles, Typography } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchArtists } from '../../store/actions/artistsActions';
import ArtistItem from './ArtistItem';

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Artists = () => {
    const classes = useStyles()
    const dispatch = useDispatch();


    const artists = useSelector(state => state.artists.artists);
    const loading = useSelector(state => state.artists.artistsLoading);

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);


    return (
        <Grid container direction="column" spacing={2}>
            <Grid item  >
                <Typography variant="h4">Artists</Typography>
            </Grid>
            <Grid item container spacing={1}>
                {loading ? (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <CircularProgress />
                        </Grid>
                    </Grid>
                ) : artists.map(artist => (
                    <ArtistItem
                        key={artist._id}
                        id={artist._id}
                        name={artist.name}
                        image={artist.image}
                        info={artist.information}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default Artists;