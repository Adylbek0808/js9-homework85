const mongoose = require('mongoose');

const AlbumSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    releaseDate: Number,
    image: String,
    artist:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Artist',
        required: true
    }
});

const Album = mongoose.model('Album', AlbumSchema);
module.exports = Album;

