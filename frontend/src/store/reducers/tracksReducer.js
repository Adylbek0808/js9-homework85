import { FETCH_TRACKS_FAILURE, FETCH_TRACKS_REQUEST, FETCH_TRACKS_SUCCESS } from "../actions/tracksActions";

const initialState = {
    album: '',
    artist: '',
    tracks: [],
    tracksLoading: false
};

const tracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACKS_REQUEST:
            return { ...state, tracksLoading: true }
        case FETCH_TRACKS_SUCCESS:
            return { ...state, tracksLoading: false, tracks: action.tracks, album: action.tracks[0].album.name, artist: action.tracks[0].album.artist };
        case FETCH_TRACKS_FAILURE:
            return { ...state, tracksLoading: false }
        default:
            return state;
    }
}

export default tracksReducer;