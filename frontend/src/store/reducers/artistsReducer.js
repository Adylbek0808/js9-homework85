import { FETCH_ARTISTS_FAILURE, FETCH_ARTISTS_REQUEST, FETCH_ARTISTS_SUCCESS } from "../actions/artistsActions";

const initialState = {
    artists: [],
    artistsLoading: false
};

const artistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ARTISTS_REQUEST:
            return { ...state, artistsLoading: true }
        case FETCH_ARTISTS_SUCCESS:
            return { ...state, artistsLoading: false, artists: action.artists };
        case FETCH_ARTISTS_FAILURE:
            return { ...state, artistsLoading: false }
        default:
            return state;
    }
}

export default artistsReducer;