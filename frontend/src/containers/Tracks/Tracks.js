import { CircularProgress, Grid, makeStyles } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchTracks } from '../../store/actions/tracksActions';

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Tracks = (props) => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const artist = useSelector(state => state.albums.artist)
    const albumNow = useSelector(state => state.tracks.album)
    const tracks = useSelector(state => state.tracks.tracks);
    const loading = useSelector(state => state.tracks.tracksLoading)

    useEffect(() => {
        dispatch(fetchTracks(props.location.search))
    }, [dispatch, props.location.search])


    return (
        <div>
            <h2>Name of Artist: {artist}</h2>
            <h4>Name of Album: {albumNow}</h4>
            {loading ? (
                <Grid container justify="center" alignItems="center" className={classes.progress}>
                    <Grid item>
                        <CircularProgress />
                    </Grid>
                </Grid>
            ) :
                (
                    <div>
                        <h3>Tracks:</h3>
                        {tracks.map(item => (
                            <div key={item._id}>
                                <p>  #{item.number} <strong>{item.name}  </strong>  {item.duration}</p>
                            </div>

                        ))}
                    </div>
                )
            }
        </div>

    );
};

export default Tracks;