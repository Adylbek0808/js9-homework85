import { FETCH_ALBUMS_FAILURE, FETCH_ALBUMS_REQUEST, FETCH_ALBUMS_SUCCESS } from "../actions/albumsActions";

const initialState = {
    artist:'',
    albums: [],
    albumsLoading: false
};

const artistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUMS_REQUEST:
            return { ...state, albumsLoading: true }
        case FETCH_ALBUMS_SUCCESS:
            return { ...state, albumsLoading: false, albums: action.albums, artist: action.albums[0].artist.name };
        case FETCH_ALBUMS_FAILURE:
            return { ...state, albumsLoading: false }
        default:
            return state;
    }
}

export default artistsReducer;