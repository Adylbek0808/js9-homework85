import { CircularProgress, Grid, makeStyles, Typography } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchAlbums } from '../../store/actions/albumsActions';
import AlbumItem from './AlbumItem';

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Albums = props => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const artistNow = useSelector(state => state.albums.artist)
    const albums = useSelector(state => state.albums.albums);
    const loading = useSelector(state => state.albums.albumsLoading)



    useEffect(() => {
        dispatch(fetchAlbums(props.location.search))
    }, [dispatch, props.location.search])


    return (
        <Grid container direction="column" spacing={2}>
            <Grid item  >
                <Typography variant="h4">Albums of: {artistNow}</Typography>
            </Grid>
            <Grid item container spacing={1}>
                {loading ? (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <CircularProgress />
                        </Grid>
                    </Grid>
                ) : albums.map(item => (
                    <AlbumItem
                        key={item._id}
                        id={item._id}
                        name={item.name}
                        image={item.image}
                        date={item.releaseDate}
                    />

                    // <div key={item._id}> {item.name} </div>
                ))}
            </Grid>
        </Grid>
    );
};

export default Albums;