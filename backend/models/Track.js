const mongoose = require('mongoose');

const TrackSchema = new mongoose.Schema({
    number:{
        type: Number,
        required:true
    },
    name: {
        type: String,
        required: true
    },
    duration: {
        type: String,
        required: true
    },
    album: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Album',
        required: true
    }
});

const Track = mongoose.model('Track', TrackSchema);
module.exports = Track;

