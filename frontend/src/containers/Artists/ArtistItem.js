import { Card, CardActionArea, CardContent, CardHeader, CardMedia, Grid,  makeStyles } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import imageNotAvailable from '../../assets/images/not_available.png'
import { apiURL } from '../../config';

const useStyles = makeStyles(theme => ({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
}))

const ArtistItem = ({ name, image, info, id }) => {
    const classes = useStyles();
    let cardImage = imageNotAvailable;
    if (image) {
        cardImage = apiURL + "/uploads/artists/" + image
    }
    return (
        <Grid item xs={12} sm={12} md={6} lg={4}>
            <Card className={classes.card}>
                <CardActionArea component={Link} to={'/albums?artist=' + id}>
                    <CardHeader title={name} />
                    <CardMedia
                        image={cardImage}
                        title={name}
                        className={classes.media}
                    />
                    <CardContent>
                        <strong style={{ marginLeft: '10px' }}>
                            {info}
                        </strong>
                    </CardContent>
                </CardActionArea>

            </Card>
        </Grid>
    );
};

export default ArtistItem;